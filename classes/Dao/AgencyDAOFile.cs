using System;
using System.Collections.Generic;
using System.IO;
using static System.Diagnostics.Process;
using System.Globalization;
using gestion_de_comptes_bancaires.classes.Dto;
using gestion_de_comptes_bancaires.classes.Services;

namespace gestion_de_comptes_bancaires.classes.Dao
{
    public class AgencyDAOFile //: IAgencyDAO
    {
     
        private static AgencyDAOFile instance;

        private AgencyDAOFile()
        {

        }

        //accesseurs



        public static AgencyDAOFile Instance
        {
            get
            {
                if (AgencyDAOFile.instance == null)
                {
                    AgencyDAOFile.instance = new AgencyDAOFile();
                }
                return AgencyDAOFile.instance;

            }
        }



       
        public Agency[] FindAll()
        {
            return AgencyFileAccess.Instance.FindAll();
        }

        public Agency FindByCode(string code)
        {
            Agency[] agencies = this.FindAll();

            foreach (Agency currentAgency in agencies)
            {
                if (currentAgency.Code == code)
                    return currentAgency;
            }

            return null;

        }

        public bool Exists(string code)
        {
            return FindByCode(code) != null;

            //if (FindByCode(code) != null)
            //    return true;
            //else
            //    return false;

        }
        public void Create(Agency newAgency)
        {

            newAgency.Code = GetNextId();

            AgencyFileAccess.Instance.Add(newAgency);

        }

        private static string GetNextId()
        {
            int nextId = AgencyCptFileAccess.Instance.NextId();
            String resultat = (nextId).ToString().PadLeft(3).Replace(" ", "0");

         
            return resultat;
        }

        public bool DeleteByCode(string code)
        {
            int i;
            Agency[] tmpAgencies = this.FindAll();
            for (i = 0; i < tmpAgencies.Length; i++)
            {
                if (tmpAgencies[i].Code == code)
                    break;


            }

            if (i < tmpAgencies.Length)
            {
                for (int j = i; j < tmpAgencies.Length - 1; j++)
                {
                    tmpAgencies[j] = tmpAgencies[j + 1];
                }
                Array.Resize(ref tmpAgencies, tmpAgencies.Length - 1);
                AgencyFileAccess.Instance.AddALL(tmpAgencies);
                return true;
            }

            return false;

        }

        public bool Modify(Agency newAgency)
        {
            Agency oldAgency = this.FindByCode(newAgency.Code);
            if (oldAgency == null)
                return false;

            Agency[] agencies = this.FindAll();

            foreach(Agency currentAgency in agencies)
            {
                if(currentAgency.Code == newAgency.Code)
                {
                    currentAgency.Name = newAgency.Name;
                    currentAgency.Address = newAgency.Address;
                    AgencyFileAccess.Instance.AddALL(agencies);
                    return true;
                }
            }

            
            return true;
        }

    }
}