using System;
using System.Collections.Generic;
using gestion_de_comptes_bancaires.classes.Dto;
using gestion_de_comptes_bancaires.classes.Dao.Interfaces;
using gestion_de_comptes_bancaires.classes.Services;

using Npgsql;
namespace gestion_de_comptes_bancaires.classes.Dao
{
    public class AgencyDaoBdd : IAgencyDAO
    {



        private NpgsqlConnection connection = Connexion.GetConnexion();

        public AgencyDaoBdd()
        {

        }

        //accesseurs

        public List<Agency> FindAll()
        {


            List<Agency> agences = new List<Agency>();
            try
            {


                connection.Open();
                string sql = "select * from agency";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, this.connection);
                NpgsqlDataReader dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    agences.Add(new Agency(dataReader.GetFieldValue<int>(0).ToString(), dataReader.GetFieldValue<string>(1), dataReader.GetFieldValue<string>(2)));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }

            return agences;
        }

        public Agency FindByCode(string code)
        {

            try
            {


                connection.Open();
                int codeInt = int.Parse(code);
                string sql = $"select * from agency where code={codeInt}";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, this.connection);
                NpgsqlDataReader dataReader = cmd.ExecuteReader();
                if (dataReader.Read())
                {
                    return new Agency(dataReader.GetFieldValue<int>(0).ToString(), dataReader.GetFieldValue<string>(1), dataReader.GetFieldValue<string>(2));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }

            return null;

        }

        public bool Exists(string code)
        {

            return FindByCode(code) != null;
        }
        public void Create(Agency newAgency)
        {

            try
            {


                connection.Open();

                string sqlNextVal = "select nextval('agency_code_seq');";
                NpgsqlCommand cmd = new NpgsqlCommand(sqlNextVal, this.connection);
                NpgsqlDataReader dataReader = cmd.ExecuteReader();
                if (dataReader.Read())
                {
                    newAgency.Code = dataReader.GetFieldValue<int>(0).ToString();
                }

                connection.Close();
                connection.Open();
                string sql = "INSERT INTO public.agency (code, \"name\", address) " +
                             " VALUES(@codeParam, @nameParam, @addressParam)";
                cmd = new NpgsqlCommand(sql, this.connection);
                cmd.Parameters.AddWithValue("codeParam", int.Parse(newAgency.Code));
                cmd.Parameters.AddWithValue("nameParam", newAgency.Name);
                cmd.Parameters.AddWithValue("addressParam", newAgency.Address);
                cmd.Prepare();
                int nbrLines = cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }

        }


        public bool DeleteByCode(string code)
        {

            try
            {


                connection.Open();


                string sql = @"update bank_account set code_agency= null where code_agency =@codeParam;
                              delete from agency a where a.code =@codeParam";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, this.connection);
                cmd.Parameters.AddWithValue("codeParam",int.Parse(code));
                cmd.Prepare();
                if (cmd.ExecuteNonQuery() > 0)
                { 
                    return true;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }
            return false;
        }

        public bool Modify(Agency newAgency)
        {


            try
            {


                connection.Open();

                string sql = $"UPDATE agency SET  \"name\"='{newAgency.Name.Replace("'", "''")}', address='{newAgency.Address.Replace("'", "''")}' where code = {newAgency.Code};";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, this.connection);
                if (cmd.ExecuteNonQuery() > 0)
                    return true;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }
            return false;
        }

    }
}