﻿using System;
using gestion_de_comptes_bancaires.classes.Dto;
using gestion_de_comptes_bancaires.classes.Dao.Interfaces;
using gestion_de_comptes_bancaires.classes.Services;
using Npgsql;
using System.Globalization;
using System.Collections.Generic;
namespace gestion_de_comptes_bancaires.classes.Dao
{
    public class ClientDaoBdd : IClientDAO
    {

        private NpgsqlConnection connection = Connexion.GetConnexion();

        public ClientDaoBdd()
        {

        }



        public List<Client> FindAll()
        {
            List<Client> clients = new List<Client>();
            try
            {


                connection.Open();
                string sql = @$"select
	                                c.id,
	                                c.last_name ,
	                                c.first_name ,
	                                to_char(c.birthdate,'dd-MM-yyyy') ,
	                                c.email
                                from
	                                client c";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, this.connection);
                NpgsqlDataReader dataReader = cmd.ExecuteReader();
                DateTime? date = null;
                while (dataReader.Read())
                {
                    if (!dataReader.IsDBNull(3))
                        date = DateTime.ParseExact(dataReader.GetFieldValue<string>(3), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    clients.Add(new Client(dataReader.GetFieldValue<string>(0), dataReader.GetFieldValue<string>(1), dataReader.GetFieldValue<string>(2), date, dataReader.GetFieldValue<string>(4)));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }

            return clients;
        }

        public void Create(Client newClient)
        {

            try
            {


                connection.Open();
                int nextVal = 0;
                string sqlNextVal = "select nextval('client_code_seq');";
                NpgsqlCommand cmd = new NpgsqlCommand(sqlNextVal, this.connection);
                NpgsqlDataReader dataReader = cmd.ExecuteReader();
                if (dataReader.Read())
                {
                    nextVal = dataReader.GetFieldValue<int>(0);
                }

                connection.Close();
                connection.Open();

                newClient.Id = GenerateClientId(nextVal);

                string sql = $"INSERT INTO public.client(id, first_name, last_name, birthdate, email) " +
                    $"VALUES(@idParam,@firstNameParam, @lastNameParam, @birthDateParam,@emailParam)";
                cmd = new NpgsqlCommand(sql, this.connection);
                cmd.Parameters.AddWithValue("idParam", newClient.Id);
                cmd.Parameters.AddWithValue("firstNameParam", newClient.FirstName);
                cmd.Parameters.AddWithValue("lastNameParam", newClient.LastName);
                cmd.Parameters.AddWithValue("birthDateParam", newClient.BirthDate);
                cmd.Parameters.AddWithValue("emailParam", newClient.Email);
                cmd.Prepare();
                cmd.ExecuteNonQuery();


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }


        }

        public Client FindById(string id)
        {


            try
            {


                connection.Open();
                string sql = @$"select
	                                c.id,
	                                c.last_name ,
	                                c.first_name ,
	                                to_char(c.birthdate,'dd-MM-yyyy') ,
	                                c.email
                                from
	                                client c
                                where id = @idParam";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, this.connection);
                cmd.Parameters.AddWithValue("@idParam", id);
                cmd.Prepare();
                NpgsqlDataReader dataReader = cmd.ExecuteReader();
                DateTime? date = null;
                while (dataReader.Read())
                {
                    if (!dataReader.IsDBNull(3))
                        date = DateTime.ParseExact(dataReader.GetFieldValue<string>(3), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    return new Client(dataReader.GetFieldValue<string>(0), dataReader.GetFieldValue<string>(1), dataReader.GetFieldValue<string>(2), date, dataReader.GetFieldValue<string>(4));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }

            return null;

        }
        public List<Client> FindByLastName(string lastName)
        {
            List<Client> clients = new List<Client>();
            try
            {


                connection.Open();
                string sql = @$"select
	                                c.id,
	                                c.last_name ,
	                                c.first_name ,
	                                to_char(c.birthdate,'dd-MM-yyyy') ,
	                                c.email
                                from
	                                client c
                                where lower(last_Name)=lower(@lastNameParam)";

                NpgsqlCommand cmd = new NpgsqlCommand(sql, this.connection);
                cmd.Parameters.AddWithValue("lastNameParam", lastName);
                cmd.Prepare();
                NpgsqlDataReader dataReader = cmd.ExecuteReader();
                DateTime? date = null;
                while (dataReader.Read())
                {

                    if (!dataReader.IsDBNull(3))
                        date = DateTime.ParseExact(dataReader.GetFieldValue<string>(3), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    clients.Add(new Client(dataReader.GetFieldValue<string>(0), dataReader.GetFieldValue<string>(1), dataReader.GetFieldValue<string>(2), date, dataReader.GetFieldValue<string>(4)));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }

            return clients;


        }

        public Client FindByIdAccount(string idAccount)
        {
            try
            {


                connection.Open();
                string sql = @$"select
	                                c.id,
	                                c.last_name ,
	                                c.first_name ,
	                                to_char(c.birthdate,'dd-MM-yyyy') ,
	                                c.email
                                from
	                                client c
                                inner join bank_account ba on
	                                ba.id_client = c.id
                                where
	                                ba.id = @idAccount;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, this.connection);
                cmd.Parameters.AddWithValue("idAccount", int.Parse(idAccount));
                cmd.Prepare();
                NpgsqlDataReader dataReader = cmd.ExecuteReader();
                DateTime? date = null;
                while (dataReader.Read())
                {
                    if (!dataReader.IsDBNull(3))
                        date = DateTime.ParseExact(dataReader.GetFieldValue<string>(3), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    return new Client(dataReader.GetFieldValue<string>(0), dataReader.GetFieldValue<string>(1), dataReader.GetFieldValue<string>(2), date, dataReader.GetFieldValue<string>(4));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }

            return null;

        }
        public bool Exists(string id)
        {
            return FindById(id) != null;
        }
        public bool DeleteById(string id)
        {
            try
            {


                connection.Open();


                string sql = @"delete from bank_account ba where ba.id_client =@idParam;
                               delete from client a where a.id =@idParam;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, this.connection);
                cmd.Parameters.AddWithValue("idParam", id);
                cmd.Prepare();
                if (cmd.ExecuteNonQuery() > 0)
                {

                    return true;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }
            return false;

        }

        public bool Modify(Client newClient)
        {
            try
            {


                connection.Open();

                string email = newClient.Email.Replace("'", "''");
                string sql = $"UPDATE public.client SET first_name=@firstNameParam, last_name=@lastNameParam, birthdate=@birthDateParam, email=@emailParam WHERE id=@idParam";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, this.connection);
                cmd.Parameters.AddWithValue("idParam", newClient.Id);
                cmd.Parameters.AddWithValue("firstNameParam", newClient.FirstName);
                cmd.Parameters.AddWithValue("lastNameParam", newClient.LastName);
                cmd.Parameters.AddWithValue("birthDateParam", newClient.BirthDate);
                cmd.Parameters.AddWithValue("emailParam", newClient.Email);
                cmd.Prepare();
                if (cmd.ExecuteNonQuery() > 0)
                    return true;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }
            return false;
        }



        private static string GenerateClientId(int val)
        {
            return "DR" + (val).ToString().PadLeft(6).Replace(" ", "0");

        }
    }
}
