﻿using System;
using System.Collections.Generic;
using gestion_de_comptes_bancaires.classes.Dto;

namespace gestion_de_comptes_bancaires.classes.Dao.Interfaces
{
    public  interface IClientDAO
    {


        public List<Client> FindAll();
        public void Create(Client newClient);
        public Client FindById(string id);
        public List<Client> FindByLastName(string lastName);
        public Client FindByIdAccount(string idAccount);
        public bool Exists(string id);
        public bool DeleteById(string id);
        public bool Modify(Client newClient);

        

       
    }
}