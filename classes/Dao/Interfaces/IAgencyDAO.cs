using System;
using System.Collections.Generic;
using System.IO;
using static System.Diagnostics.Process;
using System.Globalization;
using gestion_de_comptes_bancaires.classes.Dto;
using gestion_de_comptes_bancaires.classes.Services;

namespace gestion_de_comptes_bancaires.classes.Dao.Interfaces
{
    public interface IAgencyDAO
    {


   
        public List<Agency> FindAll();
        public Agency FindByCode(string code);
        public bool Exists(string code);
        public void Create(Agency newAgency);
        public bool DeleteByCode(string code);
        public bool Modify(Agency newAgency);

    }
}