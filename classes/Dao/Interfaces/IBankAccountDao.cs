using System;
using gestion_de_comptes_bancaires.classes.Dto;
using gestion_de_comptes_bancaires.classes.exceptions;
using System.Collections.Generic;

namespace gestion_de_comptes_bancaires.classes.Dao.Interfaces
{
    public interface IBankAccountDao
    {

        public List<BankAccount> FindAll();

        public BankAccount FindById(string id);
        public List<BankAccount> FindByIdClient(string idClient);
        public List<BankAccount> FindByIdClient(string idClient, bool lazy);


        public bool ExistsAccount(string idClient, string type);

        public int CountAccountByClient(string idClient);
        public void Create(BankAccount newBankAccount);
        public bool DeleteById(string id);

        public bool Modify(BankAccount newBankAccount);

    }

}