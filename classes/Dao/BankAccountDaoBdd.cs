using System;
using gestion_de_comptes_bancaires.classes.Dto;
using System.Collections.Generic;
using gestion_de_comptes_bancaires.classes.Dao.Interfaces;
using gestion_de_comptes_bancaires.classes.Services;
using gestion_de_comptes_bancaires.classes.exceptions;
using IoC;
using Npgsql;
using System.Globalization;
namespace gestion_de_comptes_bancaires.classes.Dao
{
    public class BankAccountDaoBdd : IBankAccountDao
    {


        private IClientDAO clientDao;
        private IAgencyDAO agencyDao;
        private NpgsqlConnection connection = Connexion.GetConnexion();



        public BankAccountDaoBdd()
        {
            this.clientDao = ApplicationContext.GetBean<IClientDAO>("ClientDaoKey");
            this.agencyDao = ApplicationContext.GetBean<IAgencyDAO>("AgencyDaoKey");
        }



        public List<BankAccount> FindAll()
        {
            List<BankAccount> bankAccounts = new List<BankAccount>();
            try
            {
                connection.Open();
                string sql = "select * from bank_account";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, this.connection);
                NpgsqlDataReader dataReader = cmd.ExecuteReader();
                string id;
                string type;
                Double balance;
                Boolean overdraft;
                string agencyCode;
                string clientId;



                while (dataReader.Read())
                {
                    id = dataReader.GetInt32(0).ToString();
                    type = !dataReader.IsDBNull(1) ? dataReader.GetString(1) : null;
                    balance = !dataReader.IsDBNull(2) ? dataReader.GetDouble(2) : 0;
                    overdraft = !dataReader.IsDBNull(3) ? dataReader.GetBoolean(3) : false;
                    agencyCode = !dataReader.IsDBNull(4) ? dataReader.GetInt32(4).ToString() : null;
                    clientId = !dataReader.IsDBNull(5) ? dataReader.GetString(5) : null;
                    bankAccounts.Add(new BankAccount(id, type, balance, overdraft, agencyCode, clientId));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }

            return bankAccounts;
        }

        public BankAccount FindById(string id)
        {
            try
            {
                connection.Open();
                string sql = "select * from bank_account ba where ba.id=@idParam";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, this.connection);
                cmd.Parameters.AddWithValue("idParam", int.Parse(id));
                cmd.Prepare();
                NpgsqlDataReader dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    return new BankAccount(dataReader.GetFieldValue<int>(0).ToString(), dataReader.GetFieldValue<string>(1), dataReader.GetFieldValue<double>(2), dataReader.GetFieldValue<Boolean>(3), dataReader.GetFieldValue<int>(4).ToString(), dataReader.GetFieldValue<string>(5));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }

            return null;
        }

        private List<BankAccount> FindByIdClientEager(string idClient)
        {
            List<BankAccount> bankAccounts = new List<BankAccount>();
            try
            {
                connection.Open();
                string sql = @"select
                                    *
                                from
                                    (bank_account ba
                                inner join client c on
                                    c.id = ba.id_client)
                                inner join

                                    Agency a

                                    on
                                ba.code_agency = a.code
                                where ba.id_client = @idClientParam";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, this.connection);
                cmd.Parameters.AddWithValue("idClientParam", idClient);
                cmd.Prepare();
                NpgsqlDataReader dataReader = cmd.ExecuteReader();
                Agency currentAgency = null;
                Client currentClient = null;
                DateTime? date = null;

                while (dataReader.Read())
                {
                    if (!dataReader.IsDBNull(9))
                        date = DateTime.ParseExact(dataReader.GetFieldValue<string>(9), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    currentClient = new Client(dataReader.GetFieldValue<string>(6),
                                    dataReader.GetFieldValue<string>(7),
                                    dataReader.GetFieldValue<string>(8),
                                    date,
                                    dataReader.GetFieldValue<string>(10));
                    currentAgency = new Agency(dataReader.GetFieldValue<int>(11).ToString(),
                                    dataReader.GetFieldValue<string>(12),
                                    dataReader.GetFieldValue<string>(13));

                    bankAccounts.Add(new BankAccount(dataReader.GetFieldValue<int>(0).ToString(),
                                    dataReader.GetFieldValue<string>(1),
                                    dataReader.GetFieldValue<double>(2),
                                    dataReader.GetFieldValue<Boolean>(3),
                                    currentAgency,
                                    currentClient));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }

            return bankAccounts;
        }
        private List<BankAccount> FindByIdClientLazy(string idClient)
        {
            List<BankAccount> bankAccounts = new List<BankAccount>();
            try
            {
                connection.Open();
                string sql = "select * from bank_account ba where ba.id_client = @idClientParam";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, this.connection);
                cmd.Parameters.AddWithValue("idClientParam", idClient);
                cmd.Prepare();
                NpgsqlDataReader dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    bankAccounts.Add(new BankAccount(dataReader.GetFieldValue<int>(0).ToString(), dataReader.GetFieldValue<string>(1), dataReader.GetFieldValue<double>(2), dataReader.GetFieldValue<Boolean>(3), dataReader.GetFieldValue<int>(4).ToString(), dataReader.GetFieldValue<string>(5)));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }

            return bankAccounts;
        }
        public List<BankAccount> FindByIdClient(string idClient)
        {
            return FindByIdClient(idClient, true);
        }
        public List<BankAccount> FindByIdClient(string idClient, bool lazy)
        {
            if (lazy)
                return FindByIdClientLazy(idClient);
            else
                return FindByIdClientEager(idClient);


        }


        public bool ExistsAccount(string idClient, string type)
        {
            try
            {


                connection.Open();

                string sqlNextVal = "select true from bank_account ba  where ba.id_client = @idClientParam and ba.\"type\" = @typeParam";
                NpgsqlCommand cmd = new NpgsqlCommand(sqlNextVal, this.connection);
                cmd.Parameters.AddWithValue("idClientParam", idClient);
                cmd.Parameters.AddWithValue("typeParam", type);
                cmd.Prepare();
                NpgsqlDataReader dataReader = cmd.ExecuteReader();
                if (dataReader.Read())
                {
                    return true;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
            finally
            {
                connection.Close();

            }

            return false;
        }


        public int CountAccountByClient(string idClient)
        {
            try
            {


                connection.Open();

                string sqlNextVal = "select count(*) from bank_account ba where ba.id_client = @idClientParam";
                NpgsqlCommand cmd = new NpgsqlCommand(sqlNextVal, this.connection);
                cmd.Parameters.AddWithValue("idClientParam", idClient);
                cmd.Prepare();
                NpgsqlDataReader dataReader = cmd.ExecuteReader();
                if (dataReader.Read())
                {
                    return dataReader.GetFieldValue<int>(0);
                }

                return 0;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return 0;
            }
            finally
            {
                connection.Close();

            }


        }

        public void Create(BankAccount newBankAccount)
        {
            if (newBankAccount == null)
            {
                throw new ArgumentNullException();
            }

            if (!this.agencyDao.Exists(newBankAccount.Agency.Code))
            {
                throw new AgencyNotFoundException("L'agence n'existe pas !!!");
            }

            if (!this.clientDao.Exists(newBankAccount.Client.Id))
            {
                throw new ClientNotFoundException("Le client n'existe pas !!!");
            }

            if (CountAccountByClient(newBankAccount.Client.Id) >= 3)
            {
                throw new InvalidTypeAccountException("Le client possede déjà 3 comptes !!!");
            }
            if (ExistsAccount(newBankAccount.Client.Id, newBankAccount.Type))
            {
                throw new InvalidTypeAccountException("Le client possede déjà un compte même type !!!");
            }

            try
            {


                connection.Open();

                string sqlNextVal = "select nextval('bank_account_id_seq');";
                NpgsqlCommand cmd = new NpgsqlCommand(sqlNextVal, this.connection);
                NpgsqlDataReader dataReader = cmd.ExecuteReader();
                if (dataReader.Read())
                {
                    newBankAccount.Id = dataReader.GetFieldValue<int>(0).ToString();
                }

                connection.Close();
                connection.Open();
                string sql = "INSERT INTO bank_account" +
                             "(id, \"type\", balance, overdraft, code_agency, id_client)" +
                             "VALUES(@idParam, @typeParam, @balanceParam, @overdraftParam, @codeAgencyParam, @idClientParam); ";
                cmd = new NpgsqlCommand(sql, this.connection);
                cmd.Parameters.AddWithValue("idParam", int.Parse(newBankAccount.Id));//peut etre int.Parse(newBankAccount.Id)
                cmd.Parameters.AddWithValue("typeParam", newBankAccount.Type);
                cmd.Parameters.AddWithValue("balanceParam", newBankAccount.Balance);
                cmd.Parameters.AddWithValue("overdraftParam", newBankAccount.Overdraft);
                cmd.Parameters.AddWithValue("codeAgencyParam", int.Parse(newBankAccount.Agency.Code));
                cmd.Parameters.AddWithValue("idClientParam", newBankAccount.Client.Id);
                cmd.Prepare();
                int nbrLines = cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }
        }
        public bool DeleteById(string id)
        {

            try
            {


                connection.Open();


                string sql = $"delete from bank_account a where a.id =@idParam";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, this.connection);
                cmd.Parameters.AddWithValue("idParam", id);
                cmd.Prepare();
                if (cmd.ExecuteNonQuery() > 0)
                {

                    return true;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }
            return false;

        }

        public bool Modify(BankAccount newBankAccount)
        {
            try
            {


                connection.Open();


                string sql = $"UPDATE public.bank_account SET overdraft=@overdraftParam, balance=@balanceParam WHERE id=@idParam";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, this.connection);
                cmd.Parameters.AddWithValue("idParam", int.Parse(newBankAccount.Id));
                cmd.Parameters.AddWithValue("balanceParam", newBankAccount.Balance);
                cmd.Parameters.AddWithValue("overdraftParam", newBankAccount.Overdraft);
                cmd.Prepare();
                if (cmd.ExecuteNonQuery() > 0)
                    return true;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.Close();

            }
            return false;
        }


    }

}