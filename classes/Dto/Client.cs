﻿using System;

namespace gestion_de_comptes_bancaires.classes.Dto
{
	public class Client
	{
		// attributs
		private String id;
		private String lastName;
		private String firstName;
		private DateTime? birthDate;
		private String email;

		// accesseurs
		public String Id { get => id; set => id = value; }
		public String LastName { get => lastName; set => lastName = value; }
		public String FirstName { get => firstName; set => firstName = value; }
		public DateTime? BirthDate { get => birthDate; set => birthDate = value; }
		public String Email { get => email; set => email = value; }

		

		// constructeurs
		public Client()
		{
		}

		public Client(string clientIdIncomplet)
		{
			this.id = clientIdIncomplet;
		}

		public Client(String clientId, String clientLastName, String clientFirstName, DateTime? clientBirthDate, String clientEmail)
		{
			this.Id = clientId;
			this.LastName = clientLastName;
			this.FirstName = clientFirstName;
			this.BirthDate = clientBirthDate;
			this.Email = clientEmail;
		}

		override
		public String ToString()
        {
			return $"ID : {Id} | Nom : {FirstName} | Prenom : {LastName} | Date de naissance : {BirthDate} | Email : {Email}";
        }

        public static Client Clone(Client oldClient)
        {
			Client newClient = new Client();
			newClient.id = oldClient.Id;
			newClient.lastName = oldClient.LastName;
			newClient.firstName = oldClient.FirstName;
			newClient.email = oldClient.Email;
			newClient.birthDate = oldClient.BirthDate;

			return newClient;
		}

		public Client Clone()
		{
			Client newClient = new Client();
			newClient.id = this.Id;
			newClient.lastName = this.LastName;
			newClient.firstName = this.FirstName;
			newClient.email = this.Email;
			newClient.birthDate = this.BirthDate;

			return newClient;
		}
	}
}

