using System;


namespace gestion_de_comptes_bancaires.classes.Dto
{
    public class Agency
	{
		//attributs
		private string code; // code à 3 chiffres
		private string name; // nom d'agence
		private string address; // adresse d'agence
        //private static int compteurCode; // code à 3 chiffres
                          
        public string Code
        {
            get
            {
                return this.code;
            }
            set
            {
                this.code = value;
            }
        }
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }
        public string Address
        {
            get
            {
                return this.address;
            }
            set
            {
                this.address = value;
            }
        }

     

        // constructeurs
        public Agency()
		{
			
		}
        public Agency(Agency other)
        {
            this.code = other.Code;
            this.name = other.Name;
            this.address = other.address;
        }

        public static Agency Clone(Agency other)
        {
            Agency newAgency = new Agency();
            newAgency.code = other.Code;
            newAgency.name = other.Name;
            newAgency.address = other.address;
            return newAgency;
            //return new Agency(other);
        }

        public Agency(string agCode)
		{
			this.code = agCode;
		}

		public Agency(string agCode, string agName, string agAddress)
		{
			this.code = agCode;
			this.name = agName;
			this.address = agAddress;
		}

		override
		public string ToString()
        {
            return $"Code : {this.code} | Nom : {this.name} | Adresse : {this.address}";
        }

        public Agency Clone()
        {
            Agency newAgency = new Agency();
            newAgency.code = this.Code;
            newAgency.name = this.Name;
            newAgency.address = this.address;
            return newAgency;
            //return new Agency(other);
        }

        public string toCsv()
        {
            return $"{this.Code};{this.Name};{this.Address}";
        }
        public static Agency Parse(String newLine,string separator)
        {
            string[] partsLine = newLine.Split(separator==null?";": separator);
           return  new Agency(partsLine[0], partsLine[1], partsLine[2]);
        }
        public static Agency Parse(String newLine)
        {

            return Agency.Parse(newLine, null);
        }
    }
}