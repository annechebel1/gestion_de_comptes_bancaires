using System;
using gestion_de_comptes_bancaires.classes.Dao;

using gestion_de_comptes_bancaires.classes.Views;
namespace gestion_de_comptes_bancaires.classes.Dto
{
	public class BankAccount
	{
        private String type;
        private String id;
        private Agency agency;
        private Client client;
        private double balance;
        private Boolean overdraft;

        public string Type { get => type; set => type = value; }
        public String Id { get => id; set => id = value; }
       
        public double Balance { get => balance; set => balance = value; }
        public Boolean Overdraft { get => overdraft; set => overdraft = value; }
        public Client Client { get => client; set => client = value; }
        internal Agency Agency { get => agency; set => agency = value; }

        public static Random randomNumber = new Random();

		public BankAccount()
		{ 
		
		
		}


		public BankAccount(string id)
        {
            this.id = id;
        }

        public BankAccount(string bankId, 
                          string bankAccountType, 
                          double balance, 
                          bool overdraft,
                          string agencyCode, 
                          string clientId)
        {
            this.Type = bankAccountType;
            this.Id = bankId;
			Agency agency = new Agency(agencyCode);
			this.Agency = agency;
			Client client = new Client(clientId);
			this.client = client;
			this.Balance = balance;
            this.Overdraft = overdraft;
        }
        public BankAccount(string bankId,
                                  string bankAccountType,
                                  double balance,
                                  bool overdraft,
                                  Agency agency,
                                  Client client)
        {
            this.Type = bankAccountType;
            this.Id = bankId;
            this.Agency = agency;
            this.client = client;
            this.Balance = balance;
            this.Overdraft = overdraft;
        }
        static String randomBankId(String bankAccountType)
		{
			String result = "";
            result = result + bankAccountType;

			for(int i = 1; i < 11; i++)
			{
				result = result + randomNumber.Next(0, 10);
			}
			return result;
		}

        override
        public String ToString()
        {
            return $" ID : {Id} "; 
        }

        public BankAccount Clone()
        {
            BankAccount newBankAccount = new BankAccount();
            newBankAccount.Id = this.id;
            newBankAccount.Type = this.type;
            newBankAccount.Balance = this.balance;
            newBankAccount.Overdraft = this.overdraft;
            newBankAccount.Agency = this.agency.Clone();//Agency.Clone(this.agency);
            newBankAccount.Client = this.client.Clone();
            return newBankAccount;
        }
        public static BankAccount Clone(BankAccount oldBankAccount)
        {
            BankAccount newBankAccount = new BankAccount();
            newBankAccount.Id = oldBankAccount.Id;
            newBankAccount.Type = oldBankAccount.Type;
            newBankAccount.Balance = oldBankAccount.Balance;
            newBankAccount.Overdraft = oldBankAccount.Overdraft;
            newBankAccount.Agency = oldBankAccount.Agency.Clone();//Agency.Clone(this.agency);
            newBankAccount.Client = oldBankAccount.Client.Clone();
            return newBankAccount;
        }
    }

}