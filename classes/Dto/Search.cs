﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gestion_de_comptes_bancaires.classes.Dto
{
    public class Search
    {
        private string choice;
        private string value;



        public Search()
        {
       
        }

        public Search(string choice, string value)
        {
            this.Choice = choice;
            this.Value = value;
        }

        public string Choice { get => choice; set => choice = value; }
        public string Value { get => value; set => this.value = value; }
    }
}
