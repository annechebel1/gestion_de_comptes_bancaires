﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;

namespace gestion_de_comptes_bancaires.classes.Services
{
    public class PropertiesReader
    {
        private Dictionary<string, string> properties = new Dictionary<string, string>();
        private string currentPath;
        public string CurrentPath { get => currentPath; }
        public Dictionary<string, string> Properties { get => properties;}

        public PropertiesReader(string path)
        {
            this.currentPath = path;
            //applicationContext
            StreamReader sr = null;

            try
            {
                sr = new StreamReader(this.currentPath);


                String newLine = "";
                string key;
                string value;
                while ((newLine = sr.ReadLine()) != null)
                {
                    newLine = newLine.Trim().Replace(" ", "");
                    if (newLine == "")
                        continue;
                    string[] partsOfNewLine = newLine.Split("=");
                    key = partsOfNewLine[0];
                    value = partsOfNewLine[1];
                    Properties.Add(key, value);
                }

                sr.Close();


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                if (sr != null)
                    sr.Close();
            }
        }



        public string GetProperty(string key)
        {

            return Properties[key];
        }
        
    }
}
