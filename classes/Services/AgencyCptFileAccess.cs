﻿using System;
using System.IO;
using gestion_de_comptes_bancaires.classes.Dto;

namespace gestion_de_comptes_bancaires.classes.Services
{
    class AgencyCptFileAccess
    {
    
    
        public const string PATH_FILE = "../../../bdd/AgenciesCpt.txt";


        private static AgencyCptFileAccess instance;

        private AgencyCptFileAccess()
        {

        }


        public static AgencyCptFileAccess Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AgencyCptFileAccess();

                }
                return instance;
            }
        }

        public int NextId()
        {

            int nextValue = 0;
            StreamReader sr = null;

            try
            {
                sr = new StreamReader(PATH_FILE);


                nextValue = int.Parse(sr.ReadLine());
                sr.Close();


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                if (sr != null)
                    sr.Close();
            }


            AddNextValue(nextValue);

            return nextValue;

        }



       

        

        private void AddNextValue(int nexValue)
        {


            StreamWriter sw = null;

            try
            {
                sw = new StreamWriter(PATH_FILE);


               
           
                    sw.WriteLine(++nexValue);
               

                sw.Close();

            }
            catch (Exception)
            {
                if (sw != null)
                    sw.Close();
            }
        }

    }
}
