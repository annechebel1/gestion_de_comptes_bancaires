﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using gestion_de_comptes_bancaires.classes.Views;
using gestion_de_comptes_bancaires.classes.Dto;
namespace gestion_de_comptes_bancaires.classes.Services
{
   



    class BankAccountFileService
    {
       
        public const string PATH_PRINT_FILE = "../../../res/";
        public const string BASE_PRINT_FILE = "Fiche_Client";
        public const string EXT_PRINT_FILE  =  ".txt";

        private static BankAccountFileService instance;

        private BankAccountFileService()
        {

        }


        public static BankAccountFileService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BankAccountFileService();

                }
                return instance;
            }
        }

        public void PrintAllBankAccountFromClient(List<BankAccount> bankAccounts)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(PATH_PRINT_FILE+BASE_PRINT_FILE+$"_{bankAccounts[0].Client.Id}_{bankAccounts[0].Client.FirstName}-{bankAccounts[0].Client.LastName}_{DateTime.Now.Ticks}"+EXT_PRINT_FILE);
                sw.Write(BankAccountView.Instance.FormatAccounts(bankAccounts));
                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                if (sw != null)
                    sw.Close();
            }

        }
    }
}
