﻿using System;
using System.Collections.Generic;
using System.Text;
using gestion_de_comptes_bancaires;
using Npgsql;
namespace gestion_de_comptes_bancaires.classes.Services
{
    public class Connexion
    {

        private static NpgsqlConnection instance;
        

        private Connexion()
        {

        }

        public static NpgsqlConnection GetConnexion()
        {
           
                if (instance == null)
                {

                string host = Program.configApp.GetProperty("host");
                string port = Program.configApp.GetProperty("port");
                string userName = Program.configApp.GetProperty("userName");
                string password = Program.configApp.GetProperty("password");
                string dataBase = Program.configApp.GetProperty("dataBase");
                string url = $"Server = {host}; Port = {port}; User id = {userName}; Password = {password}; database = {dataBase}";
                    instance = new NpgsqlConnection(url);
                }
                return instance;
                    
            
        }
    }
}
