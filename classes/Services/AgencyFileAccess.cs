﻿using System;
using System.IO;
using gestion_de_comptes_bancaires.classes.Dto;

namespace gestion_de_comptes_bancaires.classes.Services
{
    class AgencyFileAccess
    {
        public static int cpt = 1;
        public const string SEPARATOR = ";";
        public const string PATH_FILE = "../../../bdd/Agencies.csv";


        private static AgencyFileAccess instance;

        private AgencyFileAccess()
        {

        }


        public static AgencyFileAccess Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AgencyFileAccess();

                }
                return instance;
            }
        }

        public Agency[] FindAll()
        {

            Agency[] agencies = new Agency[0];
            StreamReader sr = null;

            try
            {
                sr = new StreamReader(PATH_FILE);


                String newLine = "";
              
                while ((newLine = sr.ReadLine()) != null)
                {
                    Array.Resize(ref agencies, agencies.Length + 1);
                    agencies[agencies.Length - 1] = Agency.Parse(newLine);
                }

                sr.Close();


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                if (sr != null)
                    sr.Close();
            }

            return agencies;

        }



        public void Add(Agency agency)
        {
            Agency[] agencies = new Agency[0];
            if (agency != null)
                agencies = new Agency[] { agency };
            AddALL(agencies, true);
        }

        public void AddALL(Agency[] agencies)
        {
            AddALL(agencies, false);
        }

        public void AddALL(Agency[] agencies, bool append)
        {


            StreamWriter sw = null;

            try
            {
                sw = new StreamWriter(PATH_FILE, append);


                String newLine = "";
                foreach (Agency currentAgency in agencies)
                {
                    newLine = currentAgency.toCsv();
                    sw.WriteLine(newLine);
                }

                sw.Close();

            }
            catch (Exception)
            {
                if (sw != null)
                    sw.Close();
            }
        }

    }
}
