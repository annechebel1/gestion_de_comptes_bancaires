﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using gestion_de_comptes_bancaires.classes.Dto;


namespace gestion_de_comptes_bancaires.classes.Views
{
    public class BankAccountView
    {
        private static BankAccountView instance;

        public static BankAccountView Instance
        {
            get
            {
                if (BankAccountView.instance == null)
                    BankAccountView.instance = new BankAccountView();

                return BankAccountView.instance;
            }
        }

        private BankAccountView()
        {

        }

        public BankAccount GetInfosAccountFromConsoleInCreate(List<BankAccount> bankAccounts,List<Agency> agencies, List<Client> clients)
        {
            // affichage
            DisplayHeader("Création d'un compte :", bankAccounts);
            BankAccount newBankAccount = new BankAccount();
            newBankAccount.Client = new Client();
            newBankAccount.Agency = new Agency();

            // saisie des nouvelles variables

            Console.WriteLine("\n\t1 - Compte courant");
            Console.WriteLine("\t2 - Livret A");
            Console.WriteLine("\t3 - Plan épargne logement");
            Console.Write("\n\t Quel type de compte bancaire voulez-vous ? ");
            newBankAccount.Type = Console.ReadLine();


            AgencyView.Instance.DisplayAll(agencies);
            Console.Write("\n\t Quel est le code de l'agence du compte bancaire ? ");
            newBankAccount.Agency.Code = Console.ReadLine();

            ClientView.Instance.DisplayAll(clients);
            Console.Write("\n\t Quel est l'id client du compte bancaire ? ");
            newBankAccount.Client.Id = Console.ReadLine();

            Console.Write("\t Saisir le solde du compte bancaire : ");
            newBankAccount.Balance = double.Parse(Console.ReadLine());
            if (newBankAccount.Balance < 0)
            {
                newBankAccount.Overdraft = true;
            }
            else
            {
                String choice = "";
                do
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("\t Découvert autorisé ? (répondre par O ou N) ");
                    Console.ResetColor();
                    choice = Console.ReadLine().ToUpper();
                } while (choice != "O" && choice != "N");
                if (choice == "O")
                {
                    newBankAccount.Overdraft = true;
                }
                else
                {
                    newBankAccount.Overdraft = false;
                }
            }


            Console.Clear();

            return newBankAccount;
        }

        public string GetInfoIdBankAccountFromConsoleInDelete(List<BankAccount> bankAccounts)
        {
            BankAccountView.Instance.DisplayHeader("Suppression du client :", bankAccounts);

            Console.Write("\t Saisir l'id du compte à supprimer : ");
            return Console.ReadLine();
        }


        public string GetInfosIdBankAccountFromConsoleInModify(List<BankAccount> bankAccounts)
        {
            string id;
            // affichage
            BankAccountView.Instance.DisplayHeader("Modification du compte :", bankAccounts);
            // choix de l'agence à modifier
            Console.Write("\n\n\t Saisir l'id du compte à modifier : ");
            id = Console.ReadLine();
            return id;
        }

        public BankAccount getInfosBankAccountFromConsoleInModify(BankAccount oldBankAccount)
        {
            string choice = "";

            BankAccount newBankAccount = oldBankAccount.Clone();//Client.Clone(oldBankAccount);
            do
            {


                Console.WriteLine();
                Console.WriteLine("\n 1 - Solde du compte : " + newBankAccount.Balance);
                Console.WriteLine(" 2 - Decouvert : " + newBankAccount.Overdraft);
                
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine();
                Console.WriteLine(" R - Revenir au menu");
                Console.ResetColor();

                do
                {
                    Console.Write("\n\t Quelle option choississez-vous ? : ");
                    choice = Console.ReadLine();
                    choice = (choice == "" ? choice : choice.ToUpper().Substring(0, 1));

                    //bool fff = condition ? val1 : val2

                } while (!"12R".Contains(choice.ToUpper().Substring(0, 1)));


                if ("R" == choice)
                {
                    break;
                }



                if (choice == "1")
                {
                    newBankAccount.Balance = HelperView.ModifyAttributDouble("Entrez le nouveau solde:");

                }
                else if (choice == "2")
                {

                    newBankAccount.Overdraft = HelperView.ModifyAttributBool("Voulez vous le 'Decouvert' :");

                }
                
                Console.Clear();

            } while (true);


            return newBankAccount;
        }

        public void DisplayHeader(string message, List<BankAccount> bankAccounts)
        {
            Console.WriteLine($"\n {message} \n");
            if (bankAccounts != null)
                this.DisplayAll(bankAccounts);
        }

       

        internal void DisplayAllBankAccountFromClient(List<BankAccount>bankAccounts)
        {
            Console.WriteLine(FormatAccounts(bankAccounts));
        }

     

        public void DisplayAll(List<BankAccount> bankAccounts)
        {
            if (bankAccounts.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(" Il n'y a aucun compte bancaire.");
                Console.ResetColor();
            }
            else
            {
                Console.WriteLine($"\n\t===========================================================================================================================================");
                Console.Write($"\t|");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($" ".PadRight(21, ' '));
                Console.ResetColor();

                Console.Write($"|");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($" ID du compte : ".PadRight(15, ' '));
                Console.ResetColor();

                Console.Write($"|");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($" Type de compte : ".PadRight(23, ' '));
                Console.ResetColor();

                Console.Write($"|");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($" Code de l'agence : ".PadRight(18, ' '));
                Console.ResetColor();

                Console.Write($"|");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($" ID du client : ".PadRight(16, ' '));
                Console.ResetColor();

                Console.Write($"|");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($" Solde : ".PadRight(11, ' '));
                Console.ResetColor();

                Console.Write($"|");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($" Découvert autorisé : ".PadRight(24, ' '));
                Console.ResetColor();

                Console.WriteLine($"|");
                Console.WriteLine($"\t|=====================|================|=======================|====================|================|===========|========================|");

                for (int i = 0; i < bankAccounts.Count; i++)
                {
                    Console.Write($"\t|");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write($" Compte bancaire n°{i + 1} ".PadRight(14, ' '));
                    Console.ResetColor();
                    Console.Write($"|");

                    Console.Write($" {bankAccounts[i].Id} ".PadRight(16, ' '));
                    Console.Write($"|");

                    if (bankAccounts[i].Type == "1")
                    {
                        Console.Write($" Compte courant ".PadRight(23, ' '));
                        Console.Write($"|");
                    }
                    else if (bankAccounts[i].Type == "2")
                    {
                        Console.Write($" Livret A ".PadRight(23, ' '));
                        Console.Write($"|");
                    }
                    else if (bankAccounts[i].Type == "3")
                    {
                        Console.Write($" Plan épargne logement ".PadRight(23, ' '));
                        Console.Write($"|");
                    }

                    Console.Write($" {bankAccounts[i].Agency.Code} ".PadRight(20, ' '));
                    Console.Write($"|");

                    Console.Write($" {bankAccounts[i].Client.Id} ".PadRight(16, ' '));
                    Console.Write($"|");

                    Console.Write($" {bankAccounts[i].Balance} ".PadRight(11, ' '));
                    Console.Write($"|");

                    if (bankAccounts[i].Overdraft == true)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write($" OUI ".PadRight(24, ' '));
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write($" NON ".PadRight(24, ' '));
                        Console.ResetColor();
                    }
                    Console.WriteLine($"|");
                }
                Console.WriteLine($"\t===========================================================================================================================================");
            }
        }
        public String getInfosResearchFromConsole(string message)
        {
            Console.Write($"	{message}? :  ");
            return Console.ReadLine();
        }


        public String FormatAccounts(List<BankAccount>bankAccounts)
        {
            string result = "Aucun compte à afficher";
            string smiley = "";

            
            if (bankAccounts.Count > 0)
            {
                result = "\n";
                result += "\n                                    Fiche client\n\n";
                result += "Numéro client : " + bankAccounts[0].Client.Id+"\n";
                result += "Nom : " + bankAccounts[0].Client.LastName + "\n";
                result += "Prénom : " + bankAccounts[0].Client.FirstName + "\n";
                result += "Date de naissance : " + bankAccounts[0].Client.BirthDate + "\n\n";
                result += "\n        --------------------------------------------------------------------------------------" + "\n";
                result += "        Liste de compte : " + "\n";
                result += "        --------------------------------------------------------------------------------------" + "\n";
                result += "        Numéro de compte :                         Solde : " + "\n";
                result += "        --------------------------------------------------------------------------------------" + "\n";

                for (int i = 0; i < bankAccounts.Count; i++)
                {
                    smiley = bankAccounts[i].Balance > 0 ? ":-)" : ":-(";
                    result+= "        " + bankAccounts[i].Id + "                                 " + bankAccounts[i].Balance + "    euros                 " + smiley + "\n";

                }
            }
            return result;
        }

    }
}
