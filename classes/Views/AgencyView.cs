﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gestion_de_comptes_bancaires.classes.Dto;
using gestion_de_comptes_bancaires.classes.Dao;

namespace gestion_de_comptes_bancaires.classes.Views
{
    public class AgencyView
    {
        private static AgencyView instance;

        public static AgencyView Instance
        {
            get
            {
                if (AgencyView.instance == null)
                    AgencyView.instance = new AgencyView();

                return AgencyView.instance;
            }
        }

        private AgencyView()
        {

        }

        public Agency GetInfosAgencyFromConsoleInCreate(List<Agency> agencies)
        {
            // affichage
            DisplayHeader("Création d'agence :", agencies);
            Agency currentAgency = new Agency();

            Console.Write("\n\t Saisir le nom de l'agence : ");
            currentAgency.Name = Console.ReadLine();
            Console.Write("\t Saisir l'adresse de l'agence : ");
            currentAgency.Address = Console.ReadLine();

            // affichage du nouveau tableau agency

            return currentAgency;
        }
        public string GetInfosCodeAgencgyFromConsoleInDelete(List<Agency> allAgencies)
        {
            AgencyView.Instance.DisplayHeader("Suppression d'agence :", allAgencies);

            Console.Write("\t Saisir le code de l'agence à supprimer : ");
            return Console.ReadLine();
        }


        public void DisplayHeader(string message, List<Agency> agencies)
        {
            Console.WriteLine($"\n {message} \n");
            this.DisplayAll(agencies);
        }

        
        public string GetInfosCodeAgencyFromConsoleInModify(List<Agency> allAgencies)
        {
            string agCode;
            // affichage
            AgencyView.Instance.DisplayHeader("Modification d'agence :", allAgencies);
            // choix de l'agence à modifier
            Console.Write("\n\n\t Saisir le code de l'agence à modifier : ");
            agCode = Console.ReadLine();
            return agCode;
        }

        public Agency GetInfosAgencyFromConsoleInModify(Agency oldAgency)
        {
            string choice = "";

            Agency newAgency = Agency.Clone(oldAgency);
            do
            {
               

                Console.WriteLine();
                Console.WriteLine(" 1 - Modifier le nom de l'agence : " + newAgency.Name);
                Console.WriteLine(" 2 - Modifier l'adresse de l'agence : " + newAgency.Address);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine();
                Console.WriteLine(" R - Revenir au menu");
                Console.ResetColor();

                do
                {
                    Console.Write("\n\t Quelle option choississez-vous ? : ");
                    choice = Console.ReadLine();
                    choice = (choice == "" ? choice : choice.ToUpper().Substring(0, 1));

                    //bool fff = condition ? val1 : val2

                } while (!"12CR".Contains(choice.ToUpper().Substring(0, 1)));


                if ("R" == choice)
                {
                    break;
                }



                if (choice == "1")
                {
                    newAgency.Name = HelperView.ModifyAttributString("Entrez le nouveau nom:");

                }
                else if (choice == "2")
                {

                    newAgency.Address = HelperView.ModifyAttributString("Entrez la nouvelle adresse :");

                }

                Console.Clear();

            } while (true);


            return newAgency;
        }




        public void DisplayAll(List<Agency> agencies)
        {
            if (agencies.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(" Il n'y a aucune agence.");
                Console.ResetColor();
            }
            else
            {
                Console.WriteLine($"\n\t========================================================================================================================");
                Console.Write($"\t|");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($" ".PadRight(14, ' '));
                Console.ResetColor();

                Console.Write($"|");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($" Code : ".PadRight(15, ' '));
                Console.ResetColor();

                Console.Write($"|");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($" Nom de l'agence : ".PadRight(31, ' '));
                Console.ResetColor();

                Console.Write($"|");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($" Adresse : ".PadRight(55, ' '));
                Console.ResetColor();

                Console.WriteLine($"|");
                Console.WriteLine($"\t|==============|===============================================|=======================================================|");

                for (int i = 0; i < agencies.Count; i++)
                {
                    Console.Write($"\t|");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write($" Agence n°{i + 1} ".PadRight(14, ' '));
                    Console.ResetColor();
                    Console.Write($"|");

                    Console.Write($" {agencies[i].Code} ".PadRight(15, ' '));
                    Console.Write($"|");

                    Console.Write($" {agencies[i].Name} ".PadRight(31, ' '));
                    Console.Write($"|");

                    Console.Write($" {agencies[i].Address} ".PadRight(55, ' '));
                    Console.WriteLine($"|");
                }
                Console.WriteLine($"\t========================================================================================================================");
            }
        }
    }
}
