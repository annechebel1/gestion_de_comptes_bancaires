﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gestion_de_comptes_bancaires.classes.Views
{
    public  class HelperView
    {
        public static void ExitPrint()
        {
            string choice = "";
            do
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("\n Taper sur R pour sortir: ");
                Console.ResetColor();
                choice = Console.ReadLine().ToUpper();
            } while (choice != "R");
        }

        public static int ModifyAttributInt(string message)
        {
            Console.Write($"\n\t {message} ");
            try
            {
                return int.Parse(Console.ReadLine());

            }
            catch (Exception)
            {
                return 0;
            }


        }

        public static double ModifyAttributDouble(string message)
        {
            Console.Write($"\n\t {message} ");
            try
            {
                return double.Parse(Console.ReadLine());

            }
            catch (Exception)
            {
                return 0;
            }


        }

        public static bool ModifyAttributBool(string message)
        {
            Console.Write($"\n\t {message}  (O ou 'Oui', autre pour 'Non')");

            return Console.ReadLine().ToUpper().StartsWith("O");




        }
        public static DateTime? ModifyAttributDate(string message)
        {
            Console.Write($"\n\t {message} ");
            try
            {
                return DateTime.Parse(Console.ReadLine());

            }
            catch (Exception)
            {
                return null;
            }

        }
        public static string ModifyAttributString(string message)
        {
            Console.Write($"\n\t {message} ");
            return Console.ReadLine();

        }

        public static void ConfirmMessage(string message)
        {
            ConfirmMessage(message, null);
        }
        public static void ConfirmMessage(string message, ConsoleColor? colorTmp)
        {
            // affichage de sortie
            ConsoleColor color = colorTmp ?? ConsoleColor.Green;

            Console.ForegroundColor = color;
            Console.WriteLine("\n" + message + "\n");
            Console.ResetColor();

            HelperView.ExitPrint();

            Console.Clear();
        }
    }
}
