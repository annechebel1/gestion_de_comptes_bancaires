﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gestion_de_comptes_bancaires.classes.Dto;
using gestion_de_comptes_bancaires.classes.Dao;

namespace gestion_de_comptes_bancaires.classes.Views
{
    public class ClientView
    {
        private static ClientView instance;

        public static ClientView Instance
        {
            get
            {
                if (ClientView.instance == null)
                    ClientView.instance = new ClientView();

                return ClientView.instance;
            }
        }

        private ClientView()
        {

        }

        public Client GetInfosClientFromConsoleInCreate(List<Client> clients)
        {
            // affichage
            DisplayHeader("Création de client :", clients);
            Client newClient = new Client();

            // saisie des nouvelles variables
            Console.Write("\n\t Saisir le nom du client : ");
            newClient.LastName = Console.ReadLine();
            Console.Write("\t Saisir le prénom du client : ");
            newClient.FirstName = Console.ReadLine();
            // création de l'id incrémenter
            // saisie des nouvelles variables
            Console.Write("\t Saisir la date de naissance du client (dd/MM/yyyy) : ");
            try
            {
                newClient.BirthDate = DateTime.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                newClient.BirthDate = null;
            }
            Console.Write("\t Saisir l'email du client : ");
            newClient.Email = Console.ReadLine();
            Console.Clear();

            // affichage du nouveau tableau agency

            return newClient;
        }

        public string GetInfoIdClientFromConsoleInDelete(List<Client> clients)
        {
            ClientView.Instance.DisplayHeader("Suppression du client :", clients);

            Console.Write("\t Saisir l'id du client à supprimer : ");
            return Console.ReadLine();
        }

        public string GetInfosIdClientFromConsoleInModify(List<Client> clients)
        {
            string id;
            // affichage
            ClientView.Instance.DisplayHeader("Modification du client :", clients);
            // choix de l'agence à modifier
            Console.Write("\n\n\t Saisir l'id du client à modifier : ");
            id = Console.ReadLine();
            return id;
        }

        public Search getInfosResearchFromConsole()
        {


            Search search = new Search();


            do
            {
                Console.Clear();
                DisplayHeader("Rechercher un Client  :", null);

                Console.WriteLine();
                Console.WriteLine("		1- Recherche par l'id du client : ");
                Console.WriteLine("		2- Recherche par le nom du client : ");
                Console.WriteLine("		3- Recherche par le numero du compte : ");

                Console.Write("			Choix :  ");
                search.Choice = Console.ReadLine();
                search.Choice = search.Choice == "" ? search.Choice : search.Choice.Substring(0, 1);
            } while (!"123".Contains(search.Choice));
            String message; //= "1" == choice ? "Id Client" : ("2" == choice ? "Nom du Client": "Numero du compte");

            if ("1" == search.Choice)
                message = "Id Client";
            else if ("2" == search.Choice)
                message = "Nom du Client";
            else
                message = "Numero du compte";

            Console.Write($"			{message}? :  ");
            search.Value = Console.ReadLine();

            return search;
        }

        public Client getInfosClientFromConsoleInModify(Client oldClient)
        {
            string choice = "";

            Client newClient = oldClient.Clone();//Client.Clone(oldClient);
            do
            {


                Console.WriteLine();
                Console.WriteLine("\n 1 - Nom de famille du client : " + newClient.LastName);
                Console.WriteLine(" 2 - Prénom du client : " + newClient.FirstName);
                Console.WriteLine(" 3 - Date de naissance du client : " + newClient.BirthDate);
                Console.WriteLine(" 4 - Adresse e-mail du client : " + newClient.Email);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine();
                Console.WriteLine(" R - Revenir au menu");
                Console.ResetColor();

                do
                {
                    Console.Write("\n\t Quelle option choississez-vous ? : ");
                    choice = Console.ReadLine();
                    choice = (choice == "" ? choice : choice.ToUpper().Substring(0, 1));

                    //bool fff = condition ? val1 : val2

                } while (!"1234CR".Contains(choice.ToUpper().Substring(0, 1)));


                if ("R" == choice)
                {
                    break;
                }



                if (choice == "1")
                {
                    newClient.LastName = HelperView.ModifyAttributString("Entrez le nouveau nom:");

                }
                else if (choice == "2")
                {

                    newClient.FirstName = HelperView.ModifyAttributString("Entrez le nouveau prenom :");

                }
                else if (choice == "3")
                {

                    newClient.BirthDate = HelperView.ModifyAttributDate("Entrez la nouvelle date de naissance (dd/MM/yyyy) : ");

                }
                else if (choice == "4")
                {

                    newClient.Email = HelperView.ModifyAttributString("Entrez la nouvelle adresse email :");

                }
                Console.Clear();

            } while (true);


            return newClient;
        }

        public void DisplayHeader(string message, List<Client> clients)
        {
            Console.WriteLine($"\n {message} \n");
            if (clients != null)
                this.DisplayAll(clients);
        }

        public void DisplayAll(List<Client> clients)
        {
            if (clients.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(" Il n'y a aucun client.");
                Console.ResetColor();
            }
            else
            {
                Console.WriteLine($"\n\t===================================================================================================================");
                Console.Write($"\t|");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($" ".PadRight(14, ' '));
                Console.ResetColor();

                Console.Write($"|");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($" Identifiant : ".PadRight(15, ' '));
                Console.ResetColor();

                Console.Write($"|");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($" Nom de famille : ".PadRight(18, ' '));
                Console.ResetColor();

                Console.Write($"|");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($" Prenom : ".PadRight(16, ' '));
                Console.ResetColor();

                Console.Write($"|");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($" Date de naissance : ");
                Console.ResetColor();

                Console.Write($"|");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($" Email : ".PadRight(24, ' '));
                Console.ResetColor();

                Console.WriteLine($"|");
                Console.WriteLine($"\t|==============|===============|==================|================|=====================|========================|");

                for (int i = 0; i < clients.Count; i++)
                {
                    Console.Write($"\t|");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write($" Client n°{i + 1} ".PadRight(14, ' '));
                    Console.ResetColor();
                    Console.Write($"|");

                    Console.Write($" {clients[i].Id} ".PadRight(15, ' '));
                    Console.Write($"|");

                    Console.Write($" {clients[i].LastName} ".PadRight(18, ' '));
                    Console.Write($"|");

                    Console.Write($" {clients[i].FirstName} ".PadRight(16, ' '));
                    Console.Write($"|");

                    Console.Write($" {clients[i].BirthDate} ");
                    Console.Write($"|");

                    Console.Write($" {clients[i].Email} ".PadRight(24, ' '));
                    Console.WriteLine($"|");
                }
                Console.WriteLine($"\t===================================================================================================================");
            }
        }
    }
}
