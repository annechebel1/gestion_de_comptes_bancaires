﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gestion_de_comptes_bancaires.classes.Views;
using gestion_de_comptes_bancaires.classes.Dto;
using gestion_de_comptes_bancaires.classes.Dao.Interfaces;
using IoC;
namespace gestion_de_comptes_bancaires.classes.Controllers
{
    public class ClientController
    {
        private static ClientController instance;
        private IClientDAO clientDAO;

        private ClientController()
        {
            clientDAO = ApplicationContext.GetBean<IClientDAO>("ClientDaoKey");
        }


        public static ClientController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ClientController();

                }
                return instance;
            }
        }




        public void Create()
        {
            Client newClient = ClientView.Instance.GetInfosClientFromConsoleInCreate(this.clientDAO.FindAll());
            this.clientDAO.Create(newClient);
            HelperView.ConfirmMessage($"Votre nouveau client { newClient.FirstName  } { newClient.LastName  } est enregistré !(id = {newClient.Id})!");
        }

        public void Delete()
        {

            do
            {
                string agCode = ClientView.Instance.GetInfoIdClientFromConsoleInDelete(this.clientDAO.FindAll());
                bool isDeleted = this.clientDAO.DeleteById(agCode);

                String message = $"Le client {agCode} a bien été supprimé";
                if (!isDeleted)
                {
                    message = " Il n'y a aucun client.";
                }
                HelperView.ConfirmMessage(message);
                Console.WriteLine("Voulez vous supprimer un autre client : O (pour Oui), N (pour Non)");
            } while (Console.ReadLine().ToUpper().StartsWith("O"));
        }



        public void Modify()
        {
            do
            {
                string id = ClientView.Instance.GetInfosIdClientFromConsoleInModify(this.clientDAO.FindAll());
                string message = $"L'agence {id} n'existe pas";

                Client oldClient = this.clientDAO.FindById(id);
                if (oldClient != null)
                {
                    Client newClient = ClientView.Instance.getInfosClientFromConsoleInModify(oldClient);
                    this.clientDAO.Modify(newClient);
                    message = $"L'agence {id} a bien été modifié";
                }

                HelperView.ConfirmMessage(message);
                Console.WriteLine("Voulez vous modifier une autre Client : O (pour Oui), N (pour Non)");
            } while (Console.ReadLine().ToUpper().StartsWith("O"));

        }

        public void Research()
        {
            do
            {
                Search search = ClientView.Instance.getInfosResearchFromConsole();
                List<Client> clients = new List<Client>();
                Client client;
                if ("1" == search.Choice)
                {
                    client = this.clientDAO.FindById(search.Value);
                    if (client != null)
                        clients = (new Client[] { client }).ToList();
                }
                else if ("2" == search.Choice)
                {
                    clients = this.clientDAO.FindByLastName(search.Value);
                }
                else
                {
                    client = this.clientDAO.FindByIdAccount(search.Value);
                    if (client != null)
                        clients = (new Client[] { client }).ToList();
                }

                ClientView.Instance.DisplayAll(clients);
                HelperView.ConfirmMessage("");
                Console.WriteLine("Voulez vous Rechercher un autre Client : O (pour Oui), N (pour Non)");
            } while (Console.ReadLine().ToUpper().StartsWith("O"));
        }

        internal void DisplayAll()
        {
            ClientView.Instance.DisplayAll(this.clientDAO.FindAll());
            HelperView.ExitPrint();
        }
    }
}

