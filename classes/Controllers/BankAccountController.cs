﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gestion_de_comptes_bancaires.classes.Views;
using gestion_de_comptes_bancaires.classes.Dto;
using gestion_de_comptes_bancaires.classes.Dao.Interfaces;
using gestion_de_comptes_bancaires.classes.Dao;
using gestion_de_comptes_bancaires.classes.exceptions;
using gestion_de_comptes_bancaires.classes.Services;
using IoC;
namespace gestion_de_comptes_bancaires.classes.Controllers
{
    public class BankAccountController
    {
        private static BankAccountController instance;

      
        private IAgencyDAO agencyDao;
        private IClientDAO clientDAO;
        private IBankAccountDao bankAccountDao;

        private BankAccountController()
        {
            this.bankAccountDao = ApplicationContext.GetBean<IBankAccountDao>("BankAccountDaoKey");
            this.clientDAO = ApplicationContext.GetBean<IClientDAO>("ClientDaoKey");
            this.agencyDao = ApplicationContext.GetBean<IAgencyDAO>("AgencyDaoKey");
        }
        

        public static BankAccountController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BankAccountController();

                }
                return instance;
            }
        }

        public void Create()
        {

            try
            {
                BankAccount newBankAccount =
                BankAccountView.Instance.GetInfosAccountFromConsoleInCreate(
                                    this.bankAccountDao.FindAll(),
                                    this.agencyDao.FindAll(),
                                     this.clientDAO.FindAll());
                this.bankAccountDao.Create(newBankAccount);
                HelperView.ConfirmMessage($"Votre nouveau compte numero { newBankAccount.Id  } est enregistré !");
            }
            catch (ClientNotFoundException e)
            {
                HelperView.ConfirmMessage($"Creation du compte impossible : { e.Message } !",ConsoleColor.Red);
            }
            catch (AgencyNotFoundException e)
            {
                HelperView.ConfirmMessage($"Creation du compte impossible : { e.Message } !", ConsoleColor.Blue);
            }
            catch (Exception e)
            {
                HelperView.ConfirmMessage($"Creation du compte impossible : { e.Message } !");
            }



        }

        public void Delete()
        {

            do
            {
                string id = BankAccountView.Instance.GetInfoIdBankAccountFromConsoleInDelete(this.bankAccountDao.FindAll());
                bool isDeleted = this.bankAccountDao.DeleteById(id);

                String message = $"Le compte {id} a bien été supprimé";
                if (!isDeleted)
                {
                    message = " Il n'y a aucun compte.";
                }
                HelperView.ConfirmMessage(message);
                Console.WriteLine("Voulez vous supprimer un autre compte : O (pour Oui), N (pour Non)");
            } while (Console.ReadLine().ToUpper().StartsWith("O"));
        }



        public void Modify()
        {
            do
            {
                string id = BankAccountView.Instance.GetInfosIdBankAccountFromConsoleInModify(this.bankAccountDao.FindAll());
                string message = $"Le compte {id} n'existe pas";

                BankAccount oldBankAccount = this.bankAccountDao.FindById(id);
                if (oldBankAccount != null)
                {
                    BankAccount newBankAccount = BankAccountView.Instance.getInfosBankAccountFromConsoleInModify(oldBankAccount);
                    this.bankAccountDao.Modify(newBankAccount);
                    message = $"Le compte {id} a bien été modifié";
                }

                HelperView.ConfirmMessage(message);
                Console.WriteLine("Voulez vous modifier un autre compte : O (pour Oui), N (pour Non)");
            } while (Console.ReadLine().ToUpper().StartsWith("O"));

        }
        public void ResearchByIdClient()
        {
            do
            {
     
                string valeur = BankAccountView.Instance.getInfosResearchFromConsole("Donnez l'id du client à chercher");

                List<BankAccount> bankAccounts = this.bankAccountDao.FindByIdClient(valeur);
                BankAccountView.Instance.DisplayAll(bankAccounts);
                HelperView.ConfirmMessage("");
                Console.WriteLine("Voulez vous Rechercher un autre compte : O (pour Oui), N (pour Non)");
            } while (Console.ReadLine().ToUpper().StartsWith("O"));
        }

        public void ResearchByIdBankAccount()
        {
            do
            {
                List<BankAccount> bankAccounts = new List<BankAccount>();
                string valeur = BankAccountView.Instance.getInfosResearchFromConsole("Donnez l'id du compte à chercher");

                BankAccount bankAccount = this.bankAccountDao.FindById(valeur);
               
                    if (bankAccount != null)
                        bankAccounts = new BankAccount[] { bankAccount }.ToList();
                

                BankAccountView.Instance.DisplayAll(bankAccounts);
                HelperView.ConfirmMessage("");
                Console.WriteLine("Voulez vous Rechercher un autre compte : O (pour Oui), N (pour Non)");
            } while (Console.ReadLine().ToUpper().StartsWith("O"));
        }


        public void PrintInfosBankAccount()
        {
            do
            {
                string valeur = BankAccountView.Instance.getInfosResearchFromConsole("Donnez l'id du client à chercher");

                List<BankAccount> bankAccounts = this.bankAccountDao.FindByIdClient(valeur,false);
                BankAccountView.Instance.DisplayAllBankAccountFromClient(bankAccounts);
                BankAccountFileService.Instance.PrintAllBankAccountFromClient(bankAccounts);

                HelperView.ConfirmMessage("");
                Console.WriteLine("Voulez vous imprimer les informations des comptes d'un autre client : O (pour Oui), N (pour Non)");
            } while (Console.ReadLine().ToUpper().StartsWith("O"));
        }

        internal void DisplayAll()
        {
            BankAccountView.Instance.DisplayAll(this.bankAccountDao.FindAll());
            HelperView.ExitPrint();
        }
    }
}
