﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gestion_de_comptes_bancaires.classes.Views;
using gestion_de_comptes_bancaires.classes.Dto;
using gestion_de_comptes_bancaires.classes.Dao.Interfaces;
using IoC;
namespace gestion_de_comptes_bancaires.classes.Controllers
{
    public class AgencyController
    {
        private static AgencyController instance;

        private IAgencyDAO agencyDao;

        private AgencyController()
        {
            this.agencyDao = ApplicationContext.GetBean<IAgencyDAO>("AgencyDaoKey") ;
        }


        public static AgencyController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AgencyController();

                }
                return instance;
            }
        }
        public void Create()
        {
            Agency newAgency = AgencyView.Instance.GetInfosAgencyFromConsoleInCreate(this.agencyDao.FindAll());
            this.agencyDao.Create(newAgency);
            HelperView.ConfirmMessage($"Votre nouvelle agence est créée (code = {newAgency.Code})!");
        }

        public void Delete()
        {
            // affichage
            do
            {
                string agCode = AgencyView.Instance.GetInfosCodeAgencgyFromConsoleInDelete(this.agencyDao.FindAll());
                bool isDeleted = this.agencyDao.DeleteByCode(agCode);

                String message = $"L'agence {agCode} a bien été supprimé";
                if (!isDeleted)
                {
                    message = " Il n'y a aucune agence.";
                }
                HelperView.ConfirmMessage(message);
                Console.WriteLine("Voulez vous supprimer une autre Agence : O (pour Oui), N (pour Non)");
            } while (Console.ReadLine().ToUpper().StartsWith("O"));
        }



        public void Modify()
        {
            do
            {
                string agCode = AgencyView.Instance.GetInfosCodeAgencyFromConsoleInModify(this.agencyDao.FindAll());
                string message = $"L'agence {agCode} n'existe pas";

                Agency oldAgency = this.agencyDao.FindByCode(agCode);
                if (oldAgency != null)
                {
                    Agency newAgency = AgencyView.Instance.GetInfosAgencyFromConsoleInModify(oldAgency);
                    this.agencyDao.Modify(newAgency);
                    message = $"L'agence {agCode} a bien été modifié";
                }

                HelperView.ConfirmMessage(message);
                Console.WriteLine("Voulez vous modifier une autre Agence : O (pour Oui), N (pour Non)");
            } while (Console.ReadLine().ToUpper().StartsWith("O"));

        }

        internal void DisplayAll()
        {
            AgencyView.Instance.DisplayAll(agencyDao.FindAll());
            HelperView.ExitPrint();
        }
    }
}
