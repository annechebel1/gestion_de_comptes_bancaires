﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gestion_de_comptes_bancaires.classes.exceptions
{
    [Serializable]
    public class InvalidTypeAccountException : Exception
    {
        public InvalidTypeAccountException() : base() { }
        public InvalidTypeAccountException(string message) : base(message) { }
        public InvalidTypeAccountException(string message, Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client.
        protected InvalidTypeAccountException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
