﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gestion_de_comptes_bancaires.classes.exceptions
{
    [Serializable]
    public class AgencyNotFoundException : Exception
    {
        public AgencyNotFoundException() : base() { }
        public AgencyNotFoundException(string message) : base(message) { }
        public AgencyNotFoundException(string message, Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client.
        protected AgencyNotFoundException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
