using System;
using System.Collections.Generic;
using System.IO;
using gestion_de_comptes_bancaires.classes.Controllers;
using gestion_de_comptes_bancaires.classes.Services;
using IoC;
namespace gestion_de_comptes_bancaires
{
    public  class Program
    {
   
        public static PropertiesReader configApp;
        // à configurer pour le relier à une base de donnée
        private static string pathFile = "../../../config/AppConfig.properties";
        static void Main(String[] args)
        {
            if (args.Length > 0)
                pathFile = args[0];

            Init(pathFile);
            Console.Clear();
            Console.WriteLine("\n Démarrage du programme...\n");
            DisplayMenu();
        }

        private static void Init(String path)
        {
            Program.configApp = new PropertiesReader(path) ;
            ApplicationContext.init();
        }

        static void DisplayMenu()
        {
            Boolean exit = false;
            int choice = 0;

            while (exit == false)
            {
                Console.Write(" =====================");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(" CDA Banque Interface ");
                Console.ResetColor();
                Console.WriteLine("=====================");

                Console.WriteLine(" |                                                              |");
                Console.WriteLine(" | 1 - Créer une agence                                         |");
                Console.WriteLine(" | 2 - Supprimer une agence                                     |");
                Console.WriteLine(" | 3 - Modifier une agence                                      |");
                Console.WriteLine(" | 4 - Créer un client                                          |");
                Console.WriteLine(" | 5 - Supprimer un client                                      |");
                Console.WriteLine(" | 6 - Modifier un client                                       |");
                Console.WriteLine(" | 7 - Créer un compte bancaire                                 |");
                Console.WriteLine(" | 8 - Supprimer un compte bancaire                             |");
                Console.WriteLine(" | 9 - Modifier un compte bancaire                              |");
                Console.WriteLine(" | 10 - Rechercher un compte                                    |");
                Console.WriteLine(" | 11 - Rechercher un client                                    |");
                Console.WriteLine(" | 12 - Afficher la liste des comptes d'un client               |");
                Console.WriteLine(" | 13 - Imprimer les infos client                               |");
                Console.WriteLine(" | 14 - Afficher la liste des agences                           |");
                Console.WriteLine(" | 15 - Afficher la liste des clients                           |");
                Console.WriteLine(" | 16 - Afficher la liste des comptes bancaires                 |");
                Console.WriteLine(" |                                                              |");

                Console.Write(" | ");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("17 - Quitter le programme");
                Console.ResetColor();

                Console.WriteLine("  				        |");

                Console.WriteLine(" |                                                              |");
                Console.WriteLine(" ================================================================");



                Console.Write("\n Action : ");
                try
                {
                    choice = int.Parse(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    choice = -3;
                }
                Console.Clear();
                switch (choice)
                {
                    case 1:
                        AgencyController.Instance.Create();
                        break;

                    case 2:
                        AgencyController.Instance.Delete();
                        break;

                    case 3:

                        AgencyController.Instance.Modify();

                        break;

                    case 4:

                        ClientController.Instance.Create();


                        break;

                    case 5:

                        ClientController.Instance.Delete();

                        break;

                    case 6:

                        ClientController.Instance.Modify();

                        break;

                    case 7:

                        BankAccountController.Instance.Create();
                   
                        break;

                    case 8:

                        BankAccountController.Instance.Delete();

                        break;

                    case 9:

                        BankAccountController.Instance.Modify();
                        
                        break;

                    case 10:

                        BankAccountController.Instance.ResearchByIdBankAccount();

                        break;

                    case 11:

                        ClientController.Instance.Research();
                        break;

                    case 12:

                        BankAccountController.Instance.ResearchByIdClient();
                        break;

                    case 13:

                        BankAccountController.Instance.PrintInfosBankAccount();
                        break;

                    case 14:

                        AgencyController.Instance.DisplayAll();
                      

                        break;

                    case 15:

                        ClientController.Instance.DisplayAll();
                     

                        break;

                    case 16:

                        BankAccountController.Instance.DisplayAll();
                        

                        break;

                    case 17:
                        Console.Clear();
                        exit = true;
                        Console.WriteLine("\n Arrêt du programme...\n");
                        break;
                }
                Console.Clear();
            }
        }



       



    }
}
