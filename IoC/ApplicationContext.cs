﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using gestion_de_comptes_bancaires.classes.Services;
namespace IoC
{
    class ApplicationContext
    {
        private static Dictionary<string, object> context = new Dictionary<string, object>();
        private const string PATH_FILE = "../../../config/ApplicationContext.properties";
        public static void init()
        {
            init(PATH_FILE);
        }
        public static void init(string path)
        {
            PropertiesReader propertiesContextApp = new PropertiesReader(path);
            string key;
            object value;
            foreach (var toto in propertiesContextApp.Properties)
            {
                key = toto.Key;
                Type type = Type.GetType(toto.Value);
                ConstructorInfo constructorInfo = type.GetConstructor(Type.EmptyTypes);
                value = constructorInfo.Invoke(new object[0]);
                context.Add(key, value);
            }

        }

        public static object GetBean(string key)
        {

            return context[key];
        }
        public static T GetBean<T>(string key)
        {

            return (T)context[key];
        }
    }
}
